package sample;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class InvestmentFrame extends JFrame
{    
   
   private static final double DEFAULT_RATE = 5;
   private static final double INITIAL_BALANCE = 1000;


   private JLabel rateLabel;
   private JTextField rateField;
   private JButton button;
   private JLabel resultLabel;
   private JPanel panel;
   private BankController control;
   private double total; 
   
  
   public InvestmentFrame()
   {  
	  createButton();
      createTextField();
      createPanel();  
   }
   private void createButton(){
	   button =new JButton("Submit");
   }
   private void createTextField()
   {
      rateLabel = new JLabel("Interest Rate: ");
      resultLabel = new JLabel("balance: " + INITIAL_BALANCE );

      final int FIELD_WIDTH = 10;
      rateField = new JTextField(FIELD_WIDTH);
      rateField.setText("" + DEFAULT_RATE);
   }
    
   public String getText(){
	    return rateField.getText();
   }
   public void setText(String text){
	   resultLabel.setText(text);
   }
   
   public void setListener(ActionListener listener) {
		button.addActionListener(listener);
	}
   
  
 
   private void createPanel()
   {
      panel = new JPanel();
      panel.add(rateLabel);
      panel.add(rateField);
      panel.add(button);
      panel.add(resultLabel);      
      add(panel);
   }


public JLabel getResultLabel() {
	return resultLabel;
}


public void setResultLabel(JLabel resultLabel) {
	this.resultLabel = resultLabel;
} 
}
