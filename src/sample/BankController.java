package sample;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import sample.InvestmentFrame;
import sample.BankAccount;
public class BankController {
	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 100;
	private static final double INITIAL_BALANCE = 1000;

	private BankAccount account;
	private InvestmentFrame frame;
	private JButton button;

	class AddInterestListener implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent event) {
				
				// TODO Auto-generated method stub
				
				double rate = Double.parseDouble(frame.getText());
	            double interest = account.getBalance() * rate / 100;
	            account.deposit(interest);
	            frame.setText("balance: " + account.getBalance());
				
			}
	}	 
	public static void main(String[] args) {
		new BankController();
		
	}
	  
	public BankController() {
		frame = new InvestmentFrame();
		button = new JButton("Add Interest");
		listener  = new AddInterestListener();
		frame.setListener(listener);
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
	    frame.setVisible(true);
	    setControl();
		
	}
	public  void setControl(){
		account = new BankAccount(INITIAL_BALANCE);
		
	}
	
	 ActionListener listener ;
	 
	// ������������͹�������������͡�ҧ㹡���Ѻ����觢����������ҧ���������� ���������ӧҹ���·���������ͧ�Դ��͡Ѻ�����µç
	// �֧�ն֧�� attribute / local variable �ͧ InvestmentFrame �Ѻ control � controller
	// �����繵���÷���Ҩҡ InvestmentFrame ������㹡�ô��Թ���� Controller 
	

}
